package com.catalog.gostcatalog;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyMaterialsActivity extends AppCompatActivity {

    DBHelper dbHelper;
    SQLiteDatabase db;

    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_materials);

        initToolbar();
        initListView();

    }

    public class MySimpleCursorAdapter extends SimpleCursorAdapter {
        //Кастомный адаптер для заполнения lvLists

        private Activity mActivity;
        private Context mContext;
        private int mLayout;
        private Cursor mCursor;
        private final LayoutInflater inflater;

        public MySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
            super(context, layout, c, from, to);
            this.mContext = context;
            this.mLayout = layout;
            this.mCursor = c;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return inflater.inflate(mLayout, null);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            super.bindView(view, context, cursor);

            TextView tv_itemName_myMaterial = view.findViewById(R.id.tv_itemName_myMaterial);
            TextView tv_itemNameID_myMaterial = view.findViewById(R.id.tv_itemNameID_myMaterial);

            final String itemName_myMaterial = tv_itemName_myMaterial.getText().toString();
            final String itemNameID_myMaterial = tv_itemNameID_myMaterial.getText().toString();

            tv_itemName_myMaterial.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initAlertCAE(MyMaterialsActivity.this, itemName_myMaterial, itemNameID_myMaterial);
                }
            });

            ImageView itemBtnRename = view.findViewById(R.id.ivRename);
            itemBtnRename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    renameItem(itemName_myMaterial, itemNameID_myMaterial);
                }
            });

            ImageView itemBtnDelete = view.findViewById(R.id.ivDelete);
            itemBtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteItem(itemName_myMaterial, itemNameID_myMaterial);
                }
            });

        }
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item.getItemId() == android.R.id.home) {                                                //Если кликают на стрелочку в Toolbar (кнопка назад)
            finish();                                                                               //выполнить onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    //==============================================================================================

    public void initToolbar() {
        //Инициализация toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);                                     //Нашел toolbar
        setSupportActionBar(toolbar);                                                               //Установил для activity поддержку toolbar
        if (getSupportActionBar() != null) {                                                        //Проверка на наличие toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);                                  //Делаю стрелку (кнопку назад) доступной
            getSupportActionBar().setDisplayShowHomeEnabled(true);                                  //Делаю стрелку (кнопку назад) видимой
        }
    }

    public void initListView() {
        //Инициализация lvMyMaterials, подключение к БД, вывод данных в lvMyMaterials

        final ListView lvMyMaterials = findViewById(R.id.listView);                                  //Нашел listView

        dbHelper = new DBHelper(this);                                                      //Создал экземпляр класса DBHelper для управдения БД
        db = dbHelper.getReadableDatabase();                                                  //Получил доступ на чтение БД
        Cursor cursor = db.rawQuery("SELECT id_material as _id, name_material FROM MATERIAL_TABLE WHERE description_material = 'users'", null);   //Выборка названий сталей из таблицы MATERIAL_TABLE
        String[] from = new String[]{"name_material", "_id"};                                       //Массив столбцов, чьи данные беру
        int[] to = new int[]{R.id.tv_itemName_myMaterial, R.id.tv_itemNameID_myMaterial};           //Массив view куда данные вставляю

        MySimpleCursorAdapter myAdapter = new MySimpleCursorAdapter(this, R.layout.item_listview_my_material, cursor, from, to);    //Создал адаптер, задал в нем шаблон отображения item-а, указал аргументы (что отображать и куда)
        lvMyMaterials.setAdapter(myAdapter);                                                         //Повесил адаптер на lvMyMaterials

        db.close();                                                                           //Закрыл подключение к БД
        dbHelper.close();                                                                           //Закрыл подключение к БД

    }

    public void initAlertCAE(Context context, final String itemName, final String itemNameID) {
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(context);                      //Создал экземпляр AlertDialog
        View v = View.inflate(context, R.layout.alert_cae_buttons, null);                      //Создал кастомный вид
        mDialogBuilder.setView(v);                                                                  //Применил кастомный вид
        mDialogBuilder.create();                                                                    //Создал AlertDialog
        TextView alertName = v.findViewById(R.id.alertName);
        alertName.setText(itemName);
        final AlertDialog alertDialog = mDialogBuilder.show();                                      //Эта строка только для того, чтоб вызвать dismiss(). Т.к у AlertDialog.Builder нет dismiss


        final Button alertBtnAnsys = v.findViewById(R.id.alertBtnAnsys);                            //Нашел кнопку alertBtnAnsys из этого AlertDialog-а
        alertBtnAnsys.setOnClickListener(new View.OnClickListener() {                               //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                    //Повесил обработчик
                openPropertyActivity(itemName, alertBtnAnsys.getText().toString(), itemNameID, "1");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

        final Button alertBtnWorkbench = v.findViewById(R.id.alertBtnWorkbench);                    //Нашел кнопку alertBtnWorkbench из этого AlertDialog-а
        alertBtnWorkbench.setOnClickListener(new View.OnClickListener() {                           //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                //Повесил обработчик
                openPropertyActivity(itemName, alertBtnWorkbench.getText().toString(), itemNameID, "2");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

        final Button alertBtnSolid = v.findViewById(R.id.alertBtnSolid);                            //Нашел кнопку alertBtnWorkbench из этого AlertDialog-а
        alertBtnSolid.setOnClickListener(new View.OnClickListener() {                               //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                    //Повесил обработчик
                openPropertyActivity(itemName, alertBtnSolid.getText().toString(), itemNameID, "3");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

    }

    public void openPropertyActivity(String nameMaterial, String nameCAE, String idMaterial, String idCAE) {
        //Инициализация метода, который собирает нужные данные и передает их в открываем активити "Параметры"

        Intent propertyActivity = new Intent(MyMaterialsActivity.this, PropertyActivity.class);//Создал Intent
        propertyActivity.putExtra("nameMaterial", nameMaterial);                              //Положил в Intent имя материала
        propertyActivity.putExtra("nameCAE", nameCAE);                                        //Положил в Intent имя CAE-среды
        propertyActivity.putExtra("idMaterial", idMaterial);                                  //Положил в Intent номер id материала
        propertyActivity.putExtra("idCAE", idCAE);                                            //Положил в Intent номер id CAE-среды
        propertyActivity.putExtra("activity", "MyMaterialActivity");                    //Положил в Intent номер id CAE-среды
        startActivity(propertyActivity);                                                            //Запуск FoundItemsActivity
    }

    public void renameItem(String itemName, final String itemID) {
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);                 //Создал экземпляр AlertDialog
        View v = View.inflate(this, R.layout.alert_rename_delete_item, null);                 //Создал кастомный вид
        mDialogBuilder.setView(v);                                                                  //Применил кастомный вид
        mDialogBuilder.create();                                                                    //Создал AlertDialog
        TextView tvAlertName_myMaterial = v.findViewById(R.id.tvAlertName_myMaterial);
        final EditText alertEdit_myMaterial = v.findViewById(R.id.alertEdit_myMaterial);
        Button alertBtn_myMaterial = v.findViewById(R.id.alertBtn_myMaterial);

        tvAlertName_myMaterial.setText("Введите новое название");                                   //Установил название для Alert-а переименования
        alertEdit_myMaterial.append(itemName);                                                      //При появлении Alert-а переименования в нем уже имя кликнутого item-а
        final AlertDialog alertDialog = mDialogBuilder.show();                                      //Эта строка только для того, чтоб вызвать dismiss(). Т.к у AlertDialog.Builder нет dismiss

        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();
        alertBtn_myMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!alertEdit_myMaterial.getText().toString().equals("")) {
                    ContentValues cvUpdateName = new ContentValues();
                    cvUpdateName.put("name_material", alertEdit_myMaterial.getText().toString());
                    db.update("MATERIAL_TABLE", cvUpdateName, "id_material = ?", new String[]{String.valueOf(itemID)});

                    db.close();                                                                         //Закрываю подключение к БД
                    dbHelper.close();                                                                   //Закрываю подключение к БД

                    initListView();                                                                     //Выполняю, чтоб применить изменения
                    alertDialog.dismiss();                                                              //Отпускаю alertDialog
                    Toast.makeText(MyMaterialsActivity.this, "Сохранено", Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(MyMaterialsActivity.this, "Введите название", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Keyboard keyboardClass = new Keyboard();
        keyboardClass.showKeyboard(this);
    }

    public void deleteItem(String itemName, final String itemID) {
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);                 //Создал экземпляр AlertDialog
        View v = View.inflate(this, R.layout.alert_rename_delete_item, null);                 //Создал кастомный вид
        mDialogBuilder.setView(v);                                                                  //Применил кастомный вид
        mDialogBuilder.create();                                                                    //Создал AlertDialog
        TextView tvAlertName_myMaterial = v.findViewById(R.id.tvAlertName_myMaterial);
        final EditText alertEdit_myMaterial = v.findViewById(R.id.alertEdit_myMaterial);
        Button alertBtn_myMaterial = v.findViewById(R.id.alertBtn_myMaterial);

        tvAlertName_myMaterial.setText("Удалить: \"" + itemName + "\"?");                           //Установил название для Alert-а переименования
        alertEdit_myMaterial.setVisibility(View.GONE);                                              //Спрятал alertEdit_myMaterial
        final AlertDialog alertDialog = mDialogBuilder.show();                                      //Эта строка только для того, чтоб вызвать dismiss(). Т.к у AlertDialog.Builder нет dismiss

        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();
        alertBtn_myMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.delete("MATERIAL_TABLE", "id_material = ?", new String[]{itemID});
                db.delete("MECHANICAL_TABLE", "material_id = ?", new String[]{itemID});
                db.delete("ANALYSIS_PROPERTY_TABLE", "cae_id = ?", new String[]{itemID});
                db.delete("PROPERTY_TABLE", "material_id = ?", new String[]{itemID});
                db.delete("UNITS_TABLE", "material_id = ?", new String[]{itemID});

                db.close();                                                                         //Закрываю подключение к БД
                dbHelper.close();                                                                   //Закрываю подключение к БД

                initListView();                                                                     //Выполняю, чтоб применить изменения
                alertDialog.dismiss();                                                              //Отпускаю alertDialog
                Toast.makeText(MyMaterialsActivity.this, "Материал удален", Toast.LENGTH_SHORT).show();
            }
        });

    }


}
