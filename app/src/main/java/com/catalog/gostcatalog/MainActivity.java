package com.catalog.gostcatalog;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.kobakei.ratethisapp.RateThisApp;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, TextWatcher {

    FragmentTransaction fragmentTransaction;
    Fragment fragmentCategory;
    Fragment fragmentEditSearch;
    String textEditSearch = "";                                                                     //Текст из edit_search
    String versionName;                                                                             //Сюда нахожу и помещаю версию приложения из пункта String versionName в Gradle
    EditText edit_search;
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbarDrawerNavigation();
        initMain();
        getVersion();
        estimateAppDialog();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        textEditSearch = s.toString();

        replaceFragment(textEditSearch);

    }

    @Override
    public void onBackPressed() {
        //Обработчик нажатия кнопки "Назад"

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!textEditSearch.equals("")) {
            edit_search.setText("");
            fragmentTransaction = getSupportFragmentManager().beginTransaction();                   //Вызвал менеджер фрагментов
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentCategory);                  //Вставляю в fragmentContainer фрагмент fragmentButtonsCategory
            fragmentTransaction.commit();                                                           //Выполнил
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.ic_nav_my_material:
                Intent myMaterialsActivity = new Intent(this, MyMaterialsActivity.class);
                startActivity(myMaterialsActivity);
                break;
            case R.id.nav_estimate:
                navMenu_GooglePlay();
                break;
            case R.id.nav_share:
                navMenu_share();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //==============================================================================================

    public void initMain() {
        //Инициализация всего-чего угодно

        fragmentCategory = new fragmentButtonsCategory();                                           //Создал экземпляр фрагмента fragmentButtonsCategory
        fragmentEditSearch = new fragmentEditSearch();                                              //Создал экземпляр фрагмента fgmtEditSearch

        fragmentTransaction = getSupportFragmentManager().beginTransaction();                       //Вызвал менеджер фрагментов
        fragmentTransaction.add(R.id.fragmentContainer, fragmentCategory);                          //Вставил в fragmentContainer фрагмент fragmentButtonsCategory
        fragmentTransaction.commit();                                                               //Выполнил

        edit_search = findViewById(R.id.edit_search);                                               //Нашел edit_search
        edit_search.addTextChangedListener(this);                                           //Вешаю на edit_search слушатель изменения текста

    }

    public void initToolbarDrawerNavigation() {
        //Инициализая Toolbar/DrawerLayout/NavigationView

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void replaceFragment(String s) {
        //Метод заменяет фрагменты в fragmentContainer в зависимости от пустоты/непустоты edit_search

        fragmentTransaction = getSupportFragmentManager().beginTransaction();                       //Вызвал менеджер фрагментов

        if (s.equals("")) {                                                                         //Если edit_search пустой
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentCategory);                  //Вставляю в fragmentContainer фрагмент fragmentButtonsCategory
            fragmentTransaction.commit();                                                           //Выполнил
        } else {                                                                                    //Если edit_search не пустой
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentEditSearch);                //Вставляю в fragmentContainer фрагмент fgmtEditSearch
            fragmentTransaction.commit();                                                           //Выполнил
        }
    }

    public void fab_add(View view) {
        //Инициализация fab_add (Кнопка добавления нового материала)

        Intent createMaterialActivity = new Intent(this, CreateMaterialActivity.class);
        createMaterialActivity.putExtra("nameToolbar", "Создание нового материала");
        startActivity(createMaterialActivity);
    }

    public void getVersion() {
        //Программная установка версии в пункт бокового меню

        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;   //Сохранил значение версии приложения
            NavigationView navigationView = findViewById(R.id.nav_view);                            //Нашел nav_view
            Menu menu = navigationView.getMenu();                                                   //Доступ к меню navigationView
            menu.findItem(R.id.nav_version_app).setTitle("Версия: " + versionName);                 //Программно задал текст (значение версии) пункту
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void navMenu_GooglePlay() {
        //Пункт бокового меню "Оценить в GooglePlay"

        try {                                                                                       //Попытка выполнения - если GooglPlay установлен на устройстве
            Intent intent_GooglePlay = new Intent(Intent.ACTION_VIEW, Uri.parse                     //Создал Intent в котором Action - это View(просмотр), и Uri - даныне дял просмотра(ссылка на какую то страницу)
                    ("market://details?id=" + getPackageName()));
            startActivity(intent_GooglePlay);
        } catch (android.content.ActivityNotFoundException exception) {                             //Перехват ошибки - если если GooglPlay не установлен на устройстве
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse                                  // и вроде как отрытие в обычном браузере
                    ("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    public void navMenu_share() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Справочник материалов для разных CAE-сред." + "\n" + "Ссылка на приложение: https://play.google.com/store/apps/details?id=com.catalog.gostcatalog";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Приложение: \"Справочник материалов для разных CAE-сред\""); //Вроде только для Gmail (там есть окошечко тема письма - вот туда и вставляется)
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Поделиться через..."));             //Заголовок всплывающего алерта с вариантами через что можно поделиться
    }

    public void estimateAppDialog() {
        RateThisApp.onCreate(this);                                                         //Мониторинг количество раз запуска приложения с момента установки
        RateThisApp.showRateDialogIfNeeded(this);                                           //Если условие выполнено (Прошло 7 дней или запущено 10 раз), будет показано диалоговое окно «Оценить это приложение»
        RateThisApp.Config config = new RateThisApp.Config(5, 5);   //Показать диалог либо через 5 дней, либо после 5 запуска
        RateThisApp.init(config);
    }


}
