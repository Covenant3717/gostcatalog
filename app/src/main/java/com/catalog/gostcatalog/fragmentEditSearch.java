package com.catalog.gostcatalog;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


public class fragmentEditSearch extends Fragment implements TextWatcher{

    public View fgmtEditSearch;                                                                     //View - fgmtEditSearch
    ListView lvEditSearch;
    DBHelper dbHelper;
    SQLiteDatabase database;
    Cursor cursor;
    SimpleCursorAdapter myAdapter;
    String textEditSearch;

    //==============================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fgmtEditSearch = inflater.inflate(R.layout.fragment_edit_search, container, false);


        initMain();

        return fgmtEditSearch;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
    @Override
    public void afterTextChanged(Editable s) {
        textEditSearch = s.toString();                                                              //Текст из edit_search

        initListView(textEditSearch);
    }

    //==============================================================================================

    public void initMain() {
        //Инициализация всего-чего угодно

        lvEditSearch = fgmtEditSearch.findViewById(R.id.listView);                                  //Нашел listView
        dbHelper = new DBHelper(getContext());                                                      //Создал экземпляр класса DBHelper для управдения БД

        EditText edit_search = getActivity().findViewById(R.id.edit_search);                        //Нашел edit_search
        edit_search.addTextChangedListener(this);                                           //Вешаю на edit_search слушатель изменения текста

        initListView(edit_search.getText().toString());                                             //
    }

    public void initListView(String s) {
        //Инициализация lvEditSearch, подключение к БД, вывод данных в lvEditSearch

        database = dbHelper.getReadableDatabase();                                                  //Получил доступ на чтение БД
        String sql = "SELECT id_material as _id, name_material FROM MATERIAL_TABLE WHERE name_material LIKE" + "'%" + s + "%'";
        cursor = database.rawQuery(sql, null);                                          //Выборка по шаблону
        String[] from = new String[]{"name_material", "_id"};                                       //Массив столбцов, чьи данные беру
        int[] to = new int[]{R.id.itemName, R.id.itemNameID};                                       //Массив view куда данные вставляю

        lvEditSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {                 //Повесил обаботчик нажатия для каждого item-а
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView itemName = view.findViewById(R.id.itemName);                               //Нашел TextView itemName в кликнутом item-е
                TextView itemNameID = view.findViewById(R.id.itemNameID);                           //Нашел TextView itemNameID в кликнутом item-е
                initAlertCAE(getContext(), itemName.getText().toString(), itemNameID.getText().toString());
            }
        });

        myAdapter = new SimpleCursorAdapter(fgmtEditSearch.getContext(), R.layout.item_listview, cursor, from, to);
        lvEditSearch.setAdapter(myAdapter);                                                         //Повесил адаптер на lvLists

        database.close();                                                                           //Закрыл подключение к БД
        dbHelper.close();                                                                           //Закрыл подключение к БД
    }

    public void initAlertCAE(Context context, final String itemName, final String itemNameID) {
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(context);                      //Создал экземпляр AlertDialog
        View v = View.inflate(context, R.layout.alert_cae_buttons, null);                      //Создал кастомный вид
        mDialogBuilder.setView(v);                                                                  //Применил кастомный вид
        mDialogBuilder.create();                                                                    //Создал AlertDialog
        TextView alertName = v.findViewById(R.id.alertName);
        alertName.setText(itemName);
        final AlertDialog alertDialog = mDialogBuilder.show();                                      //Эта строка только для того, чтоб вызвать dismiss(). Т.к у AlertDialog.Builder нет dismiss


        final Button alertBtnAnsys = v.findViewById(R.id.alertBtnAnsys);                            //Нашел кнопку alertBtnAnsys из этого AlertDialog-а
        alertBtnAnsys.setOnClickListener(new View.OnClickListener() {                               //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                    //Повесил обработчик
                openPropertyActivity(itemName, alertBtnAnsys.getText().toString(), itemNameID, "1");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

        final Button alertBtnWorkbench = v.findViewById(R.id.alertBtnWorkbench);                    //Нашел кнопку alertBtnWorkbench из этого AlertDialog-а
        alertBtnWorkbench.setOnClickListener(new View.OnClickListener() {                           //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                //Повесил обработчик
                openPropertyActivity(itemName, alertBtnWorkbench.getText().toString(), itemNameID, "2");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

        final Button alertBtnSolid = v.findViewById(R.id.alertBtnSolid);                            //Нашел кнопку alertBtnWorkbench из этого AlertDialog-а
        alertBtnSolid.setOnClickListener(new View.OnClickListener() {                               //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                    //Повесил обработчик
                openPropertyActivity(itemName, alertBtnSolid.getText().toString(), itemNameID,"3");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

    }

    public void openPropertyActivity(String nameMaterial, String nameCAE, String idMaterial, String idCAE) {
        //Инициализация метода, который собирает нужные данные и передает их в открываем активити "Параметры"

        Intent propertyActivity = new Intent(getContext(), PropertyActivity.class);                 //Создал Intent
        propertyActivity.putExtra("nameMaterial", nameMaterial);                              //Положил в Intent имя материала
        propertyActivity.putExtra("nameCAE", nameCAE);                                        //Положил в Intent имя CAE-среды
        propertyActivity.putExtra("idMaterial", idMaterial);                                  //Положил в Intent номер id материала
        propertyActivity.putExtra("idCAE", idCAE);                                            //Положил в Intent номер id CAE-среды
        startActivity(propertyActivity);                                                            //Запуск FoundItemsActivity
    }

}
