package com.catalog.gostcatalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CreateMaterialActivity extends AppCompatActivity implements View.OnClickListener {

    DBHelper dbHelper;
    SQLiteDatabase db;

    EditText edNameNewMaterial;
    EditText ed_value_prop_1;
    EditText ed_value_prop_2;
    EditText ed_value_prop_3;
    EditText ed_value_prop_4;
    EditText ed_value_prop_5;

    ArrayList<TextView> arrList_tvCaption = new ArrayList<TextView>();
    ArrayList<TextView> arrList_tvUnits = new ArrayList<TextView>();
    ArrayList<EditText> arrList_editValue = new ArrayList<EditText>();
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_material);
//        setTitle("Создание нового материала");

        initToolbar();
        initMain();
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item.getItemId() == android.R.id.home) {                                                //Если кликают на стрелочку в Toolbar (кнопка назад)
            finish();                                                                               //выполнить onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
    }
    //==============================================================================================

    public void initToolbar() {
        //Инициализация toolbar
        Bundle extras = getIntent().getExtras();                                                    //Получил Bundle (пакет параметров)
        if (extras != null) {                                                                       //Если пакет не пустой
            setTitle(extras.getString("nameToolbar"));                                         //Вытаскивая название и вставляю в Toolbar
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);                                     //Нашел toolbar
        setSupportActionBar(toolbar);                                                               //Установил для activity поддержку toolbar
        if (getSupportActionBar() != null) {                                                        //Проверка на наличие toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);                                  //Делаю стрелку (кнопку назад) доступной
            getSupportActionBar().setDisplayShowHomeEnabled(true);                                  //Делаю стрелку (кнопку назад) видимой
        }
    }

    public void initMain() {
        //Иницизация всего, чего угодно

        Keyboard keyboardClass = new Keyboard();
        keyboardClass.hideKeyboard(this);

        edNameNewMaterial = findViewById(R.id.edNameNewMaterial);
        ed_value_prop_1 = findViewById(R.id.ed_value_prop_1);
        ed_value_prop_2 = findViewById(R.id.ed_value_prop_2);
        ed_value_prop_3 = findViewById(R.id.ed_value_prop_3);
        ed_value_prop_4 = findViewById(R.id.ed_value_prop_4);
        ed_value_prop_5 = findViewById(R.id.ed_value_prop_5);

        Bundle extras = getIntent().getExtras();                                                    //Получил Bundle (пакет параметров)
        if (extras != null) {                                                                       //Если пакет не пустой идем дальше
            if (extras.getString("nameMaterial") != null)                                      //Если ключ "nameMaterial" не пустой идем дальше
                edNameNewMaterial.setText(extras.getString("nameMaterial"));                   //Установил текст из ключа "nameMaterial" в edNameNewMaterial

            if (extras.getString("UngaValue") != null)                                         //Если ключ "UngaValue" не пустой идем дальше
                ed_value_prop_1.setText(extras.getString("UngaValue"));                        //Установил текст из ключа "UngaValue" в ed_value_prop_1

            if (extras.getString("PuassonValue") != null)                                      //Если ключ "PuassonValue" не пустой идем дальше
                ed_value_prop_2.setText(extras.getString("PuassonValue"));                     //Установил текст из ключа "PuassonValue" в ed_value_prop_2

            if (extras.getString("DensityValue") != null)                                      //Если ключ "DensityValue" не пустой идем дальше
                ed_value_prop_3.setText(extras.getString("DensityValue"));                     //Установил текст из ключа "DensityValue" в ed_value_prop_3

            if (extras.getString("ProchnostValue") != null)                                    //Если ключ "ProchnostValue" не пустой идем дальше
                ed_value_prop_4.setText(extras.getString("ProchnostValue"));                   //Установил текст из ключа "ProchnostValue" в ed_value_prop_4

            if (extras.getString("TekuchestValue") != null)                                    //Если ключ "TekuchestValue" не пустой идем дальше
                ed_value_prop_5.setText(extras.getString("TekuchestValue"));                   //Установил текст из ключа "TekuchestValue" в ed_value_prop_5
        }
    }

    public void fabCreateComplete(View view) {
        //Метод, описывающий добавление нового материала в БД самим пользователем

        String value_prop_1 = ed_value_prop_1.getText().toString();
        String value_prop_2 = ed_value_prop_2.getText().toString();
        String value_prop_3 = ed_value_prop_3.getText().toString();
        String value_prop_4 = ed_value_prop_4.getText().toString();
        String value_prop_5 = ed_value_prop_5.getText().toString();

        if (!edNameNewMaterial.getText().toString().equals("")) {                                   //Проверка 1: edNameNewMaterial должен быть не пуст
            //----------Тут идет добавление в БД пяти основных параметров
            if (!value_prop_1.equals("") && !value_prop_2.equals("") && !value_prop_3.equals("") && !value_prop_4.equals("") && !value_prop_5.equals("")) { //Проверка 2: на пустоту эдитов, в которые пишутся значения параметров
                dbHelper = new DBHelper(this);                                              //Создал экземпляр класса DBHelper для управдения БД
                db = dbHelper.getWritableDatabase();                                                //Получил доступ на чтение и запись БД

                ContentValues cvMATERIAL_TABLE = new ContentValues();                                   //ContentValues для таблицы MATERIAL_TABLE
                cvMATERIAL_TABLE.put("name_material", edNameNewMaterial.getText().toString());          //Положил имя нового материала в cvMATERIAL_TABLE
                cvMATERIAL_TABLE.put("description_material", "users");                                  //Положил описание (о том, что создан пользователем) нового материала в cvMATERIAL_TABLE
                long rowID = db.insert("MATERIAL_TABLE", null, cvMATERIAL_TABLE);  //Добавил в таблицу MATERIAL_TABLE и получил id присвоеное материалу

                ContentValues cvMECHANICAL_TABLE = new ContentValues();                             //ContentValues для таблицы MECHANICAL_TABLE
                cvMECHANICAL_TABLE.put("material_id", rowID);                                       //Положил id нового материала
                cvMECHANICAL_TABLE.put("property_id", 1);                                           //Положил номер(идентификатор) параметра, которому ща буду присваивать значение
                cvMECHANICAL_TABLE.put("value", Double.parseDouble(value_prop_1));                  //Положил значение
                db.insert("MECHANICAL_TABLE", null, cvMECHANICAL_TABLE);       //Добавил в таблицу MECHANICAL_TABLE
                cvMECHANICAL_TABLE.clear();                                                         //Очистил cvMECHANICAL_TABLE

                cvMECHANICAL_TABLE.put("material_id", rowID);
                cvMECHANICAL_TABLE.put("property_id", 2);
                cvMECHANICAL_TABLE.put("value", Double.parseDouble(value_prop_2));
                db.insert("MECHANICAL_TABLE", null, cvMECHANICAL_TABLE);
                cvMECHANICAL_TABLE.clear();

                cvMECHANICAL_TABLE.put("material_id", rowID);
                cvMECHANICAL_TABLE.put("property_id", 3);
                cvMECHANICAL_TABLE.put("value", Double.parseDouble(value_prop_3));
                db.insert("MECHANICAL_TABLE", null, cvMECHANICAL_TABLE);
                cvMECHANICAL_TABLE.clear();

                cvMECHANICAL_TABLE.put("material_id", rowID);
                cvMECHANICAL_TABLE.put("property_id", 4);
                cvMECHANICAL_TABLE.put("value", Double.parseDouble(value_prop_4));
                db.insert("MECHANICAL_TABLE", null, cvMECHANICAL_TABLE);
                cvMECHANICAL_TABLE.clear();

                cvMECHANICAL_TABLE.put("material_id", rowID);
                cvMECHANICAL_TABLE.put("property_id", 5);
                cvMECHANICAL_TABLE.put("value", Double.parseDouble(value_prop_5));
                db.insert("MECHANICAL_TABLE", null, cvMECHANICAL_TABLE);
                cvMECHANICAL_TABLE.clear();

                //----------Тут идет добавление пользовательских параметров
                if (arrList_tvCaption.size() != 0) {
                    for (int i = 0; i < arrList_tvCaption.size(); i++) {
                        ContentValues cvUNITS_TABLE = new ContentValues();                          //ContentValues для таблицы UNITS_TABLE
                        cvUNITS_TABLE.put("material_id", rowID);                                    //Id-добавляемой-строки будет равно id-материала
                        cvUNITS_TABLE.put("name_units", ((TextView) arrList_tvUnits.get(i)).getText().toString());//Положил единицы измерения i-го параметра
                        long idRowUNITS_TABLE = db.insert("UNITS_TABLE", null, cvUNITS_TABLE);//Добавил строку в таблицу UNITS_TABLE и узнал ее Id
                        cvUNITS_TABLE.clear();                                                      //Очистил cvUNITS_TABLE

                        ContentValues cvPROPERTY_TABLE = new ContentValues();                       //ContentValues для таблицы PROPERTY_TABLE
                        cvPROPERTY_TABLE.put("material_id", rowID);                                 //Id-добавляемой-строки будет равно id-материала
                        cvPROPERTY_TABLE.put("name_property", ((TextView) arrList_tvCaption.get(i)).getText().toString());//Положил название i-го параметра
                        cvPROPERTY_TABLE.put("units_id", idRowUNITS_TABLE);                         //Положил значение id i-го параметра
                        long idRowPROPERTY_TABLE = db.insert("PROPERTY_TABLE", null, cvPROPERTY_TABLE);//Добавил строку в таблицу PROPERTY_TABLE и узнал ее Id
                        cvPROPERTY_TABLE.clear();                                                   //Очистил cvPROPERTY_TABLE

                        ContentValues cvMECHANICAL_TABLE_users = new ContentValues();               //ContentValues для таблицы MECHANICAL_TABLE
                        cvMECHANICAL_TABLE_users.put("material_id", rowID);                         //Положил id нового материала
                        cvMECHANICAL_TABLE_users.put("property_id", idRowPROPERTY_TABLE);           //Положил номер(идентификатор) параметра, которому ща буду присваивать значение
                        cvMECHANICAL_TABLE_users.put("value", ((EditText) arrList_editValue.get(i)).getText().toString()); //Положил значение
                        db.insert("MECHANICAL_TABLE", null, cvMECHANICAL_TABLE_users);//Добавил в таблMECHANICAL_TABLE
                        cvMECHANICAL_TABLE_users.clear();                                           //Очистил cvMECHANICAL_TABLE

                        ContentValues cvANALYSIS_PROPERTY_TABLE = new ContentValues();              //ContentValues для таблицы ANALYSIS_PROPERTY_TABLE
                        cvANALYSIS_PROPERTY_TABLE.put("property_id", idRowPROPERTY_TABLE);          //Положил id-значение параметра из таблицы PROPERTY_TABLE
                        cvANALYSIS_PROPERTY_TABLE.put("cae_id", rowID);                             //Положил в столбец cae_id айдишник материала,
                        cvANALYSIS_PROPERTY_TABLE.put("caption_property", ((TextView) arrList_tvCaption.get(i)).getText().toString());//Положил Положил название i-го параметра
                        cvANALYSIS_PROPERTY_TABLE.put("weight", 6 + i);                             //Положил вес (он инкрементируется, сначала полюбому будет шестой, а дальше шестой плюс значение счетчика)
                        db.insert("ANALYSIS_PROPERTY_TABLE", null, cvANALYSIS_PROPERTY_TABLE);//Добавил в табл ANALYSIS_PROPERTY_TABLE
                        cvANALYSIS_PROPERTY_TABLE.clear();                                          //Очистил cvANALYSIS_PROPERTY_TABLE
                    }
                }

                db.close();
                dbHelper.close();

                Toast.makeText(this, "Сохранено в \"Мои материалы\"", Toast.LENGTH_SHORT).show();
                finish();                                                                           //выполнить onBackPressed();
            } else {
                Toast.makeText(this, "Заполнены не все параметры", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Введите название", Toast.LENGTH_SHORT).show();
        }
    }

    public void fabAddNewProperty(View view) {
        //Обработка нажатия кнопки fabAddNewProperty

        Keyboard keyboardClass = new Keyboard();
        keyboardClass.showKeyboard(this);

        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);                 //Создал экземпляр AlertDialog
        View v = View.inflate(this, R.layout.alert_add_new_property, null);            //Создал кастомный вид
        mDialogBuilder.setView(v);                                                                  //Применил кастомный вид
        mDialogBuilder.create();                                                                    //Создал AlertDialog
        final AlertDialog alertDialog = mDialogBuilder.show();                                      //Эта строка только для того, чтоб вызвать dismiss(). Т.к у AlertDialog.Builder нет dismiss

        final EditText edCaptionNewProperty = v.findViewById(R.id.edCaptionNewProperty);            //Нашел edCaptionNewProperty
        final EditText edUnitsNewProperty = v.findViewById(R.id.edUnitsNewProperty);                //Нашел edUnitsNewProperty
        final EditText edValueNewProperty = v.findViewById(R.id.edValueNewProperty);                //Нашел edValueNewProperty

        Button alertBtnNewMaterial = v.findViewById(R.id.alertBtnNewMaterial);                      //Нашел alertBtnNewMaterial
        alertBtnNewMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                                              //Повесил обработчик нажатий
                if (!edCaptionNewProperty.getText().toString().equals("") && !edUnitsNewProperty.getText().toString().equals("") && !edValueNewProperty.getText().toString().equals("")) {
                    final LinearLayout containerProperty = findViewById(R.id.containerProperty);    //Нашел containerProperty
                    final LinearLayout itemNewProperty = (LinearLayout) getLayoutInflater().inflate(R.layout.item_new_property, null);    //Создал itemNewProperty из xml-файла
                    containerProperty.addView(itemNewProperty);                                     //Добавил itemNewProperty в containerProperty

                    final TextView captionNewProperty = itemNewProperty.findViewById(R.id.captionNewProperty);
                    captionNewProperty.setText(edCaptionNewProperty.getText().toString());          //Установил в captionNewProperty текст из edCaptionNewProperty
                    arrList_tvCaption.add(captionNewProperty);                                      //Добавил в массив

                    final TextView unitsNewProperty = itemNewProperty.findViewById(R.id.unitsNewProperty);
                    unitsNewProperty.setText(edUnitsNewProperty.getText().toString());              //Установил в unitsNewProperty текст из edUnitsNewProperty
                    arrList_tvUnits.add(unitsNewProperty);                                          //Добавил в массив

                    final EditText editNewProperty = itemNewProperty.findViewById(R.id.editNewProperty);
                    editNewProperty.setText(edValueNewProperty.getText().toString());               //editNewProperty в unitsNewProperty текст из edValueNewProperty
                    arrList_editValue.add(editNewProperty);                                         //Добавил в массив

                    ImageView ivDeleteNewProperty = itemNewProperty.findViewById(R.id.ivDeleteNewProperty);//Нашел ivDeleteNewProperty
                    ivDeleteNewProperty.setOnClickListener(new View.OnClickListener() {             //Повесил обработчик нажатий
                        @Override
                        public void onClick(View v) {
                            containerProperty.removeView(itemNewProperty);                          //Удаление свойства при нажатии иконки-корзина
                            arrList_tvCaption.remove(captionNewProperty);
                            arrList_tvUnits.remove(unitsNewProperty);
                            arrList_editValue.remove(editNewProperty);
                        }
                    });

                    editNewProperty.requestFocus();                                                 //Фокус в в editNewProperty только что созданного параметра
                    alertDialog.dismiss();                                                          //Отпускаю alertDialog
//                    Toast.makeText(CreateMaterialActivity.this, "Параметр добавлен", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CreateMaterialActivity.this, "Заполнены не все параметры", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
