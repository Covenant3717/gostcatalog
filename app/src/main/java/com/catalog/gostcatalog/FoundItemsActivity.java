package com.catalog.gostcatalog;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class FoundItemsActivity extends AppCompatActivity {

    Bundle extras;                                                                                  //Пакет параметров переданный фрагментом fragmentButtonsCategory
    DBHelper dbHelper;
    SQLiteDatabase database;

    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found_items);

        initMain();
        initToolbar();
        initListView();
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item.getItemId() == android.R.id.home) {                                                //Если кликают на стрелочку в Toolbar (кнопка назад)
            finish();                                                                               //выполнить onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    //==============================================================================================

    public void initToolbar() {
        //Инициализация toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);                                     //Нашел toolbar
        setSupportActionBar(toolbar);                                                               //Установил для activity поддержку toolbar
        if (getSupportActionBar() != null) {                                                        //Проверка на наличие toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);                                  //Делаю стрелку (кнопку назад) доступной
            getSupportActionBar().setDisplayShowHomeEnabled(true);                                  //Делаю стрелку (кнопку назад) видимой
        }
    }

    public void initMain() {

    }

    public void initListView() {
        //Инициализация lvEditSearch, подключение к БД, вывод данных в lvEditSearch

        final ListView lvEditSearch = findViewById(R.id.listView);                                  //Нашел listView
        dbHelper = new DBHelper(this);                                                      //Создал экземпляр класса DBHelper для управдения БД
        database = dbHelper.getReadableDatabase();                                                  //Получил доступ на чтение БД
        Cursor cursor = null;                                                                       //Создал cursor, но заполняю его ниже (в операторе swich)

        extras = getIntent().getExtras();                                                           //Получил Bundle (пакет параметров)
        if (extras != null) {                                                                       //Если пакет не пустой
            String nameCategory = extras.getString("nameCategory");
            switch (nameCategory) {                                                                 //Достаю оттуда текст по ключу "nameCategory"
                case "Стали":
                    cursor = database.rawQuery("SELECT id_material as _id, name_material FROM MATERIAL_TABLE WHERE name_material LIKE '%Сталь%' ", null);   //Выборка названий сталей из таблицы MATERIAL_TABLE
                    setTitleActivity(cursor.getCount());
                    break;
                case "Чугуны":
                    cursor = database.rawQuery("SELECT id_material as _id, name_material FROM MATERIAL_TABLE WHERE name_material LIKE '%Чугун%' ", null);   //Выборка названий сталей из таблицы MATERIAL_TABLE
                    setTitleActivity(cursor.getCount());
                    break;
            }
        }

        String[] from = new String[]{"name_material", "_id"};                                       //Массив столбцов, чьи данные беру
        int[] to = new int[]{R.id.itemName, R.id.itemNameID};                                       //Массив view куда данные вставляю

        lvEditSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {                 //Повесил обаботчик нажатия для каждого item-а
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView itemName = view.findViewById(R.id.itemName);                               //Нашел TextView itemName в кликнутом item-е
                TextView itemNameID = view.findViewById(R.id.itemNameID);                           //Нашел TextView itemNameID в кликнутом item-е
                initAlertCAE(FoundItemsActivity.this, itemName.getText().toString(), itemNameID.getText().toString());
            }
        });

        SimpleCursorAdapter myAdapter = new SimpleCursorAdapter(this, R.layout.item_listview, cursor, from, to);    //Создал адаптер, задал в нем шаблон отображения item-а, указал аргументы (что отображать и куда)
        lvEditSearch.setAdapter(myAdapter);                                                         //Повесил адаптер на lvEditSearch

        database.close();                                                                           //Закрыл подключение к БД
        dbHelper.close();                                                                           //Закрыл подключение к БД
    }

    public void setTitleActivity(int count) {
        extras = getIntent().getExtras();                                                           //Получил Bundle (пакет параметров)
        if (extras != null) {                                                                       //Если пакет не пустой
            dbHelper = new DBHelper(this);                                                  //Создал экземпляр класса DBHelper для управдения БД
            database = dbHelper.getReadableDatabase();                                              //Получил доступ на чтение БД
            setTitle(extras.getString("nameCategory") + ": " + count);                         //Достаю оттуда текст по ключу "nameCategory" и вставляю это название в Toolbar
        }

    }

    public void initAlertCAE(Context context, final String itemName, final String itemNameID) {
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(context);                      //Создал экземпляр AlertDialog
        View v = View.inflate(context, R.layout.alert_cae_buttons, null);                      //Создал кастомный вид
        mDialogBuilder.setView(v);                                                                  //Применил кастомный вид
        mDialogBuilder.create();                                                                    //Создал AlertDialog
        TextView alertName = v.findViewById(R.id.alertName);
        alertName.setText(itemName);
        final AlertDialog alertDialog = mDialogBuilder.show();                                      //Эта строка только для того, чтоб вызвать dismiss(). Т.к у AlertDialog.Builder нет dismiss


        final Button alertBtnAnsys = v.findViewById(R.id.alertBtnAnsys);                            //Нашел кнопку alertBtnAnsys из этого AlertDialog-а
        alertBtnAnsys.setOnClickListener(new View.OnClickListener() {                               //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                    //Повесил обработчик
                openPropertyActivity(itemName, alertBtnAnsys.getText().toString(), itemNameID, "1");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

        final Button alertBtnWorkbench = v.findViewById(R.id.alertBtnWorkbench);                    //Нашел кнопку alertBtnWorkbench из этого AlertDialog-а
        alertBtnWorkbench.setOnClickListener(new View.OnClickListener() {                           //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                //Повесил обработчик
                openPropertyActivity(itemName, alertBtnWorkbench.getText().toString(), itemNameID, "2");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

        final Button alertBtnSolid = v.findViewById(R.id.alertBtnSolid);                            //Нашел кнопку alertBtnWorkbench из этого AlertDialog-а
        alertBtnSolid.setOnClickListener(new View.OnClickListener() {                               //Повесил на нее обработчик нажатий
            @Override
            public void onClick(View v) {                                                    //Повесил обработчик
                openPropertyActivity(itemName, alertBtnSolid.getText().toString(), itemNameID, "3");
                alertDialog.dismiss();                                                              //Скрыть AlertDialog
            }
        });

    }

    public void openPropertyActivity(String nameMaterial, String nameCAE, String idMaterial, String idCAE) {
        //Инициализация метода, который собирает нужные данные и передает их в открываем активити "Параметры"

        Intent propertyActivity = new Intent(FoundItemsActivity.this, PropertyActivity.class);//Создал Intent
        propertyActivity.putExtra("nameMaterial", nameMaterial);                              //Положил в Intent имя материала
        propertyActivity.putExtra("nameCAE", nameCAE);                                        //Положил в Intent имя CAE-среды
        propertyActivity.putExtra("idMaterial", idMaterial);                                  //Положил в Intent номер id материала
        propertyActivity.putExtra("idCAE", idCAE);                                            //Положил в Intent номер id CAE-среды
        startActivity(propertyActivity);                                                            //Запуск FoundItemsActivity
    }

}
