package com.catalog.gostcatalog;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PropertyActivity extends AppCompatActivity {

    SimpleCursorAdapter myAdapter;
    ListView lvPropertyActivity;

    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);
        setTitle("Параметры");

        initToolbar();
        initMain();
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:                                                                 //Если кликают на стрелочку в Toolbar (кнопка назад)
                finish();                                                                           //выполнить onBackPressed();
                break;
            case R.id.action_copy_material:
                copyMaterial();
                break;
            case R.id.action_translate_solid:
                translatePropertyForSolidworks();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_property_activity, menu);                             //Активирую меню
        MenuItem action_copy_material = menu.findItem(R.id.action_copy_material);                   //Нашел пункт "Скопировать материал"
        MenuItem action_translate_solid = menu.findItem(R.id.action_translate_solid);               //Нашел пункт "Перевод параметров для Solidworks"

        Bundle extras = getIntent().getExtras();                                                    //Получил Bundle (пакет параметров)
        if (extras != null) {                                                                       //Если пакет не пустой
            String idCAE = extras.getString("idCAE");                                          //Достал номер CAE-среды

            if (extras.getString("activity") != null) {                                        //Если этот ключ не пустой тогда
                String activity = extras.getString("activity");                                //Достал имя activity которая открыла это "окно свойств"
                switch (activity) {
                    case "MyMaterialActivity":                                                      //Если ее имя MyMaterialActivity
                        switch (idCAE) {
                            case "1":                                                               //Для сае-Ansys
                                action_copy_material.setVisible(false);                             //Прячу пункт "Скопировать материал"
                                action_translate_solid.setVisible(false);                           //Прячу пункт "Перевод параметров для Solidworks"
                                break;
                            case "2":                                                               //Для сае-AnsysWorkbench
                                action_copy_material.setVisible(false);                             //Прячу пункт "Скопировать материал"
                                action_translate_solid.setVisible(false);                           //Прячу пункт "Перевод параметров для Solidworks"
                                break;
                            case "3":                                                               //Для сае-Solid
                                action_copy_material.setVisible(false);                             //Прячу пункт "Скопировать материал"
                                action_translate_solid.setVisible(true);                            //Показываю пункт "Перевод параметров для Solidworks"
                                break;
                        }
                        break;
                }

            } else {                                                                                //Иначе
                switch (idCAE) {
                    case "1":                                                                       //Для сае-Ansys
                        action_translate_solid.setVisible(false);                                   //Прячу пункт "Перевод параметров для Solidworks"
                        break;
                    case "2":                                                                       //Для сае-AnsysWorkbench
                        action_translate_solid.setVisible(false);                                   //Прячу пункт "Перевод параметров для Solidworks"
                        break;
                    case "3":                                                                       //Для сае-Solid
                        action_translate_solid.setVisible(true);                                    //Показываю пункт "Перевод параметров для Solidworks"
                        break;
                }
            }
        }
        return true;
    }


    //==============================================================================================

    public void initToolbar() {
        //Инициализация toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);                                     //Нашел toolbar
        setSupportActionBar(toolbar);                                                               //Установил для activity поддержку toolbar
        if (getSupportActionBar() != null) {                                                        //Проверка на наличие toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);                                  //Делаю стрелку (кнопку назад) доступной
            getSupportActionBar().setDisplayShowHomeEnabled(true);                                  //Делаю стрелку (кнопку назад) видимой
        }
    }

    public void initMain() {
        Bundle extras = getIntent().getExtras();                                                    //Получил Bundle (пакет параметров)
        if (extras != null) {                                                                       //Если пакет не пустой
            TextView tvPropNameMaterial = findViewById(R.id.tvPropNameMaterial);                    //Нашел tvPropNameMaterial
            TextView tvPropNameCAE = findViewById(R.id.tvPropNameCAE);                              //Нашел tvPropNameCAE

            tvPropNameMaterial.setText(extras.getString("nameMaterial"));                      //Достал имя материала и усановил в tvPropNameMaterial
            tvPropNameCAE.setText(extras.getString("nameCAE"));                                //Достал имя CAE-среды и усановил в tvPropNameCAE
            String idMaterial = extras.getString("idMaterial");                                //Достал id номер материала
            String idCAE = extras.getString("idCAE");                                          //Достал номер CAE-среды

            initListView(idMaterial, idCAE);
        }
    }

    public void initListView(String idMaterial, String idCAE) {
        //Инициализация lvEditSearch, подключение к БД, вывод данных в lvEditSearch

        lvPropertyActivity = findViewById(R.id.listView);                                           //Нашел listView
//        lvPropertyActivity.setDivider(null);

        DBHelper dbHelper = new DBHelper(this);                                             //Создал экземпляр класса DBHelper для управдения БД
        SQLiteDatabase database = dbHelper.getReadableDatabase();                                   //Получил доступ на чтение БД

        String table =
                "((PROPERTY_TABLE inner join UNITS_TABLE on PROPERTY_TABLE.units_id = UNITS_TABLE.id_units)" +
                        "PROPERTY_TABLE inner join MECHANICAL_TABLE on PROPERTY_TABLE.id_property = MECHANICAL_TABLE.property_id)" +
                        "PROPERTY_TABLE inner join ANALYSIS_PROPERTY_TABLE on PROPERTY_TABLE.id_property = ANALYSIS_PROPERTY_TABLE.property_id";

        String columns[] = {
                "id_property as _id",
                "PROPERTY_TABLE.material_id as ID_PROPERTY_TABLE",
                "UNITS_TABLE.material_id as ID_UNITS_TABLE",
                "MECHANICAL_TABLE.material_id as ID_MECHANICAL_TABLE",
                "name_property",
                "name_units",
                "value",
                "caption_property"
        };

        String selection = "ID_MECHANICAL_TABLE = ? and cae_id = ? or cae_id = ?";
        String[] selectionArgs = {idMaterial, idCAE, idMaterial};
        String groupBy = null;
        String having = null;
        String orderBy = "weight";

        Cursor cursor = database.query(table, columns, selection, selectionArgs, groupBy, having, orderBy); //Выборка
        String[] from = new String[]{"caption_property", "name_property", "name_units", "value"};   //Массив столбцов, чьи данные беру
        int[] to = new int[]{R.id.lvPropCaptionProperty, R.id.lvPropDescriptionProperty, R.id.lvPropUnitsProperty, R.id.lvPropValueProperty};//Массив view куда данные вставляю
        myAdapter = new SimpleCursorAdapter(this, R.layout.item_listview_property_activity, cursor, from, to);    //Создал адаптер, задал в нем шаблон отображения item-а, указал аргументы (что отображать и куда)
        lvPropertyActivity.setAdapter(myAdapter);                                                   //Повесил адаптер на lvPropertyActivity

        database.close();                                                                           //Закрыл подключение к БД
        dbHelper.close();                                                                           //Закрыл подключение к БД

    }

    public void copyMaterial() {
        TextView tvPropNameMaterial = findViewById(R.id.tvPropNameMaterial);                        //Имя материала
        String PropNameMaterial = tvPropNameMaterial.getText().toString();                          //Имя материала

        String UngaValue = null;
        String PuassonValue = null;
        String DensityValue = null;
        String ProchnostValue = null;
        String TekuchestValue = null;

        for (int i = 0; i <= lvPropertyActivity.getChildCount() - 1; i++) {                              //Цикл по параметрам материала
            LinearLayout item = (LinearLayout) lvPropertyActivity.getChildAt(i);                    //Взял i-ый элемент
            String lvPropDescriptionProperty = ((TextView) item.findViewById(R.id.lvPropDescriptionProperty)).getText().toString();  //Взял lvPropDescriptionProperty i-ого элемента (это его описание "Description")
            TextView lvPropValueProperty = (TextView) item.findViewById(R.id.lvPropValueProperty);  //Нашел lvPropValueProperty i-ого элемента (это его значение "Value")
            switch (lvPropDescriptionProperty) {
                case "Модуль Юнга":                                                                 //Распихиваю значения по соответсвию модуль Юнга - в эдиттекстЮнга и т.д
                    UngaValue = lvPropValueProperty.getText().toString();
                    break;
                case "Коэффициент Пуассона":
                    PuassonValue = lvPropValueProperty.getText().toString();
                    break;
                case "Предел прочности":
                    ProchnostValue = lvPropValueProperty.getText().toString();
                    break;
                case "Предел текучести":
                    TekuchestValue = lvPropValueProperty.getText().toString();
                    break;
                case "Плотность":
                    DensityValue = lvPropValueProperty.getText().toString();
                    break;
            }


        }

        Intent createMaterialActivity = new Intent(this, CreateMaterialActivity.class);
        createMaterialActivity.putExtra("nameToolbar", "Создание копии материала");     //Название для Toolbar
        createMaterialActivity.putExtra("nameMaterial", PropNameMaterial);                    //Название для материала
        createMaterialActivity.putExtra("UngaValue", UngaValue);                              //Значение для Юнга
        createMaterialActivity.putExtra("PuassonValue", PuassonValue);                        //Значение для Пуассона
        createMaterialActivity.putExtra("DensityValue", DensityValue);                        //Значение для Плотности
        createMaterialActivity.putExtra("ProchnostValue", ProchnostValue);                    //Значение для Прочности
        createMaterialActivity.putExtra("TekuchestValue", TekuchestValue);                    //Значение для Текучести
        startActivity(createMaterialActivity);
    }

    public void translatePropertyForSolidworks() {
        for (int i = 0; i < lvPropertyActivity.getChildCount(); i++) {
            TextView lvPropCaptionProperty = ((View) lvPropertyActivity.getChildAt(i)).findViewById(R.id.lvPropCaptionProperty);
            String itemCaption = lvPropCaptionProperty.getText().toString();

            switch (itemCaption) {
                //Перевод на английский
                case "Модуль упругости":
                    lvPropCaptionProperty.setText("Elastic Modulus");
                    break;
                case "Коэффициент Пуассона":
                    lvPropCaptionProperty.setText("Poisson`s Ratio");
                    break;
                case "Предел прочности при растяжении":
                    lvPropCaptionProperty.setText("Tensile Strength");
                    break;
                case "Предел текучести":
                    lvPropCaptionProperty.setText("Yield Strength");
                    break;
                case "Массовая плотность":
                    lvPropCaptionProperty.setText("Mass Density");
                    break;

                //Перевод на Русский
                case "Elastic Modulus":
                    lvPropCaptionProperty.setText("Модуль упругости");
                    break;
                case "Poisson`s Ratio":
                    lvPropCaptionProperty.setText("Коэффициент Пуассона");
                    break;
                case "Tensile Strength":
                    lvPropCaptionProperty.setText("Предел прочности при растяжении");
                    break;
                case "Yield Strength":
                    lvPropCaptionProperty.setText("Предел текучести");
                    break;
                case "Mass Density":
                    lvPropCaptionProperty.setText("Массовая плотность");
                    break;
            }
        }
        Toast.makeText(this, "Ru/En перевод", Toast.LENGTH_SHORT).show();
    }

    void logCursor(Cursor c) {
        // вывод в лог данных из курсора

        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    Log.d("myLog", str);
                } while (c.moveToNext());
            }
        } else
            Log.d("myLog", "Cursor is null");
    }

}
