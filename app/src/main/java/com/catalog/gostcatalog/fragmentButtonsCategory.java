package com.catalog.gostcatalog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class fragmentButtonsCategory extends Fragment implements View.OnClickListener{

    public static View fgmtCategoty;                                                                //View - fgmtCategoty

    //==============================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fgmtCategoty = inflater.inflate(R.layout.fragment_buttons_category, container, false);

        initButtonsCategory();
        initMain();


        return fgmtCategoty;
    }

    @Override
    public void onClick(View v) {
        //Слушатель всех view находящихся в fgmtCategoty

        switch (v.getId()){
            case R.id.btn_steel:
                openBtnSteelCategory();
                break;
            case R.id.btn_cast_iron:
                openBtnCastIronCategory();
                break;
        }

    }
    //==============================================================================================

    public void initMain() {
        Button btn_steel = fgmtCategoty.findViewById(R.id.btn_steel);                               //Нашел btnSteel
        btn_steel.setOnClickListener(this);                                                         //Повесил обработчик нажатий

        Button btn_cast_iron = fgmtCategoty.findViewById(R.id.btn_cast_iron);                       //Нашел btn_cast_iron
        btn_cast_iron.setOnClickListener(this);                                                     //Повесил обработчик нажатий
    }

    public void initButtonsCategory() {
        //Инициализая кнопок-категорий (Сталь, Чугун и т.д.)

        final Button btn_steel = fgmtCategoty.findViewById(R.id.btn_steel);                         //Нашел btn_steel
        final Button btn_cast_iron = fgmtCategoty.findViewById(R.id.btn_cast_iron);                 //Нашел btn_cast_iron

        btn_steel.post(new Runnable() {                                                             //Получил доступ к btn_steel после отрисовки макета
            @Override
            public void run() {
                int widthValue = btn_steel.getWidth();                                              //Узнал ширину btn_steel
                btn_steel.setHeight(widthValue);                                                    //Установил высоту btn_steel равную ширине btn_steel
                btn_cast_iron.setHeight(widthValue);                                                //Установил высоту btn_cast_iron равную ширине btn_steel
            }
        });
    }

    public void openBtnSteelCategory() {
        //Инициализация плитки "Стали" (открытие списка имеющихся сталей)

        Intent foundItemsActivity = new Intent(getContext(), FoundItemsActivity.class);             //Создал Intent
        Button btnSteel = fgmtCategoty.findViewById(R.id.btn_steel);                                //Нашел кнопку нажатой категории по id
        foundItemsActivity.putExtra("nameCategory", btnSteel.getText().toString());           //Положил в Intent имя нажатой категории
        startActivity(foundItemsActivity);                                                          //Запуск FoundItemsActivity
    }

    public void openBtnCastIronCategory() {
        //Инициализация плитки "Стали" (открытие списка имеющихся сталей)

        Intent foundItemsActivity = new Intent(getContext(), FoundItemsActivity.class);             //Создал Intent
        Button btn_cast_iron = fgmtCategoty.findViewById(R.id.btn_cast_iron);                       //Нашел btn_cast_iron
        foundItemsActivity.putExtra("nameCategory", btn_cast_iron.getText().toString());      //Положил в Intent имя нажатой категории
        startActivity(foundItemsActivity);                                                          //Запуск FoundItemsActivity
    }


}
